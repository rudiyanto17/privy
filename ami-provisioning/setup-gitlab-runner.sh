sleep 10
sudo yum install git -y
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm"
sudo rpm -i gitlab-runner_amd64.rpm

sudo usermod -aG docker gitlab-runner
sudo setfacl -m user:gitlab-runner:rw /var/run/docker.sock
