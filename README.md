---
title: RUDIYANTO privy test
description: This repo is only for testing purposes
---

## Step-00: Introduction 
1. Create EKS Cluster
3. Create Private EKS Node Group
4. build AMI automation using packer
5. Provissioning VM 
6. set up gitlab ci

## Step-1: export your acces and secret key aws.
- **Create your access key and secret first:** 
```t
aws configure

```
- **and fill all the prompt:** 


## Step-2: Terraform apply vpc and eks
- **clone this repo first and follow along:** 
```t
cd vpc-eks
terraform init
terraform plan
terraform apply --auto-approve

```
## Step-3: Build aws AMI using packer 
- **copy the nat gateway and copas on nginx.conf** 
```t
cd ../ami-provisioning
vim ngin.conf
```
```t
* copy NAT gateway eip in nginx.conf*

server {
    listen       80;
	allow NAT_IP;
	deny all;
    server_name  _;
	location /
	{
        proxy_pass http://127.0.0.1:5000;
	    allow NAT_ip;
	    deny all;
	}
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }

```
```t
packer build ami-privy.json

```
## Step-4: Provissioning virtual mechine and will be using AMI that has already built
```t
cd ../virtual-mechine-template
vim ec2-privy-variables.tf
```
```t
* adjust the vpc id that whe have created and ami id that we have created and subnet id which is public ip that we alrdy set up before*

variable "instance_type" {
  description = "EC2 Instance Type"
  type = string
  default = "t2.micro"  
}

# AWS EC2 Instance Key Pair
variable "instance_keypair" {
  description = "AWS EC2 Key pair that need to be associated with EC2 Instance"
  type = string
  default = "YOUR_KEY"
}

variable  "instance_name" {
  description = "for instance name or tags"
  type = string
  default = "privy"

}

variable "ami" {
  description = "customized ami"
  type = string 
  default = "AMI_ID"
}

variable "subnet_id" {
  description = "subnet id from vpc"
  type = string
  default = "YOUR_PUBLIC_SUBNET"
}

variable "vpc_id" {
  description = "vpc id"
  type = string
  default = "YOUR_VPC_ID"
}

```

```t
terraform plan
terraform apply --auto-aprove

```
## Step-5: Git lab ci
1. add variable on gitlab setting > CICD > variables
2. add variables for CI_REGISTRY_IMAGE, CI_REGISTRY_PASSWORD, and CI_REGISTRY_USER 
3. edit some file and push to git repository.


## Step-6:  Testing
```t
* update kube config first*
aws eks --region ap-southeast-1 update-kubeconfig --name rudi-project-dev-privy
cd vpc-eks/deployment
kubectl create -f curl.yaml
kubectl exec -it curl /bin/sh
curl http://NAT_IP


```

## Step-7:  Implement HPA for limitting the POD
```t
cd deployment/metrics-server
kubectl apply -f .

```
### check it should be the metrics server deployed in your cluster
```t
kubectl get pods --all-namespaces

```
#### The output must be simillar like this
```t
NAMESPACE     NAME                             READY   STATUS    RESTARTS   AGE
kube-system   aws-node-982kv                   1/1     Running   0          14m
kube-system   aws-node-rqbg9                   1/1     Running   0          13m
kube-system   coredns-86d9946576-9k6gx         1/1     Running   0          25m
kube-system   coredns-86d9946576-m67h6         1/1     Running   0          25m
kube-system   kube-proxy-lcklc                 1/1     Running   0          13m
kube-system   kube-proxy-tk96q                 1/1     Running   0          14m
kube-system   metrics-server-9f459d97b-q5989   1/1     Running   0          41s

```

#### check the service of metrics-server that already deployed
```t
$ kubectl get svc -n kube-system
NAME             TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)         AGE
kube-dns         ClusterIP   10.100.0.10             53/UDP,53/TCP   26m
metrics-server   ClusterIP   10.100.66.231           443/TCP         82s

```
#### check the pod of the metrics-server
```t
$ kubectl top pods -n kube-system
NAME                             CPU(cores)   MEMORY(bytes)
aws-node-982kv                   4m           40Mi
aws-node-rqbg9                   5m           39Mi
coredns-86d9946576-9k6gx         2m           8Mi
coredns-86d9946576-m67h6         2m           8Mi
kube-proxy-lcklc                 1m           11Mi
kube-proxy-tk96q                 1m           11Mi
metrics-server-9f459d97b-q5989   3m           15Mi

```

```t
cd ..
$ kubectl apply -f hpa-deployment.yaml
deployment.apps/hpa-demo-deployment created

$ kubectl get pods
NAME                                   READY   STATUS    RESTARTS   AGE
hpa-demo-deployment-6b988776b4-b2hkb   1/1     Running   0          20s

```

```t
$ kubectl apply -f service.yaml
service/hpa-demo-deployment created

$ kubectl get svc
NAME                  TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
hpa-demo-deployment   ClusterIP   10.100.124.139           80/TCP    7s
kubernetes            ClusterIP   10.100.0.1               443/TCP   172m

$ kubectl apply -f hpa.yaml
horizontalpodautoscaler.autoscaling/hpa-demo-deployment created

$ kubectl get hpa
NAME                  REFERENCE                      TARGETS  MINPODS MAXPODS REPLICAS   AGE
hpa-demo-deployment   Deployment/hpa-demo-deployment 0%/50%    1       10      0          8s

```
#### Run this command for stress test the deployment
```t
kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://hpa-demo-deployment; done"

```
#### watch the hpa in our deployment the replicas should be increased by hpa
```t
$ kubectl get hpa -w
NAME                REFERENCE                      TARGETS MINPODS MAXPODS REPLICAS AGE
hpa-demo-deployment Deployment/hpa-demo-deployment  0%/50%  1      10      2        15m
...
...

```

## Step-8: Cleaning
```t
cd virtual-mechine-template
terraform destroy --auto-approve
cd ../vpc-eks
terraform destroy --auto-approve
```